package ru.mrchuvyzgalov.hw8.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import ru.mrchuvyzgalov.hw8.exception.ApplicationExceptions.ApplicationException;
import ru.mrchuvyzgalov.hw8.exception.ApplicationExceptions.ApplicationException.ApplicationExceptionCompanion;

import static ru.mrchuvyzgalov.hw8.exception.ApplicationExceptions.ADDING_STUDENT_NOT_POSSIBLE;

@ControllerAdvice
public class ExceptionHandlingControllerAdvice {
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ResponseEntity<ApplicationExceptionCompanion> onGradeConstraintValidationException(MethodArgumentNotValidException e) {
        return handleApplicationException(ADDING_STUDENT_NOT_POSSIBLE.exception());
    }

    @ExceptionHandler(ApplicationException.class)
    public ResponseEntity<ApplicationExceptionCompanion> handleApplicationException(ApplicationException e) {
        return ResponseEntity.status(e.companion.code()).body(e.companion);
    }
}
