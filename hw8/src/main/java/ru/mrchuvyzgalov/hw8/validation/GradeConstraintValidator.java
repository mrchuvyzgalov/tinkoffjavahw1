package ru.mrchuvyzgalov.hw8.validation;

import ru.mrchuvyzgalov.hw8.model.Course;
import ru.mrchuvyzgalov.hw8.model.Student;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class GradeConstraintValidator implements ConstraintValidator<GradeConstraint, Course> {

    @Override
    public boolean isValid(Course course, ConstraintValidatorContext constraintValidatorContext) {
        if (course.getStudents() == null) {
            return true;
        }
        for (Student student : course.getStudents()) {
            if (course.getReqiredGrade() > student.getGrade()) {
                return false;
            }
        }
        return true;
    }
}
