package ru.mrchuvyzgalov.hw8.dao;

import org.apache.ibatis.annotations.Mapper;
import ru.mrchuvyzgalov.hw8.model.Course;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Mapper
public interface CourseRepository {
    void save(Course course);

    Optional<Course> findById(UUID id);
    Optional<Course> findByIdWithoutStudents(UUID id);

    List<Course> findAll();
    Optional<Course> findCourseWithTheHighestAverageAgeOfStudents();

    void updateById(Course course);

    void deleteById(UUID id);
    void deleteAll();
}
