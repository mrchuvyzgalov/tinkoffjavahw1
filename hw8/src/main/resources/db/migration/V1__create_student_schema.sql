CREATE TABLE students
(
    id UUID PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    age INTEGER NOT NULL,
    grade INTEGER NOT NULL
);

CREAtE TABLE courses
(
    id UUID PRIMARY KEY,
    title VARCHAR(256) NOT NULL,
    description VARCHAR(256) NOT NULL,
    reqired_grade INTEGER NOT NULL
);

CREATE TABLE students_courses
(
    student_id UUID,
    course_id UUID,
    FOREIGN KEY (student_id) REFERENCES students(id) ON DELETE CASCADE,
    FOREIGN KEY (course_id) REFERENCES courses(id) ON DELETE CASCADE,
    PRIMARY KEY (student_id, course_id)
);

