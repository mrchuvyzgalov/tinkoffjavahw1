package ru.mrchuvyzgalov.hw8;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.junit.jupiter.Testcontainers;
import ru.mrchuvyzgalov.hw8.cache.CourseCache;

import java.io.IOException;
import java.sql.SQLException;

@SpringBootTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@ContextConfiguration(initializers = AbstractTest.class)
@Testcontainers
public class AbstractTest implements ApplicationContextInitializer<ConfigurableApplicationContext> {
    @Autowired
    private CourseCache courseCache;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @AfterEach
    void resetPostgres() throws SQLException {
        jdbcTemplate.execute("delete from students_courses");
        jdbcTemplate.execute("delete from students");
        jdbcTemplate.execute("delete from courses");

        courseCache.clear();
    }


    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {

    }

    public static byte[] convertObjectToJsonBytes(Object object) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsBytes(object);
        } catch (IOException exc) {
            throw new RuntimeException("Failed to build json: " + exc);
        }
    }
}
