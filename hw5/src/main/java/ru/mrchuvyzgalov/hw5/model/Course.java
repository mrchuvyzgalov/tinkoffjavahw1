package ru.mrchuvyzgalov.hw5.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Course {
    private UUID id;
    private String title;
    private String description;
    private Set<Student> students;
}
