package ru.mrchuvyzgalov.hw5.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.mrchuvyzgalov.hw5.dao.CourseRepository;
import ru.mrchuvyzgalov.hw5.model.Course;

import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class CourseService {
    private final CourseRepository courseRepository;

    public void save(Course course) {
        courseRepository.save(course);
    }

    public Course findById(UUID id) {
        return courseRepository.findById(id).orElseThrow();
    }

    public List<Course> findAll() {
        return courseRepository.findAll();
    }

    public Course findCourseWithTheHighestAverageAgeOfStudents() {
        return courseRepository.findCourseWithTheHighestAverageAgeOfStudents().orElseThrow();
    }

    public void updateById(Course course) {
        courseRepository.updateById(course);
    }

    public void deleteById(UUID id) {
        courseRepository.deleteById(id);
    }

    public void deleteAll() {
        courseRepository.deleteAll();
    }
}
