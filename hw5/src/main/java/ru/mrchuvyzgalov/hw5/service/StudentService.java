package ru.mrchuvyzgalov.hw5.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.mrchuvyzgalov.hw5.dao.StudentRepository;
import ru.mrchuvyzgalov.hw5.model.Student;

import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class StudentService {
    private final StudentRepository studentRepository;

    public void save(Student student) {
        studentRepository.save(student);
    }

    public Student findById(UUID id) {
        return studentRepository.findById(id).orElseThrow();
    }

    public List<Student> findAll() {
        return studentRepository.findAll();
    }

    public void updateById(Student student) {
        studentRepository.updateById(student);
    }

    public void deleteById(UUID id) {
        studentRepository.deleteById(id);
    }

    public void deleteAll() {
        studentRepository.deleteAll();
    }
}
