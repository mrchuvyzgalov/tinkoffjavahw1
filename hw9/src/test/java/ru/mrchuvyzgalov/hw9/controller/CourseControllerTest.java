package ru.mrchuvyzgalov.hw9.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.web.servlet.MockMvc;
import ru.mrchuvyzgalov.hw9.AbstractTest;
import ru.mrchuvyzgalov.hw9.cache.CourseCache;
import ru.mrchuvyzgalov.hw9.dao.CourseRepository;
import ru.mrchuvyzgalov.hw9.model.Course;
import ru.mrchuvyzgalov.hw9.model.Student;
import ru.mrchuvyzgalov.hw9.service.CourseService;
import ru.mrchuvyzgalov.hw9.service.StudentService;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static ru.mrchuvyzgalov.hw9.exception.ApplicationExceptions.*;

@AutoConfigureMockMvc
public class CourseControllerTest extends AbstractTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private CourseCache courseCache;
    @Autowired
    private CourseService courseService;
    @Autowired
    private CourseRepository courseRepository;
    @Autowired
    private StudentService studentService;

    @Test
    void testCreateCourseSuccess() throws Exception {
        Course course = new Course(UUID.fromString("bde8452f-ea24-44ca-9f0f-e99258b105d7"), "Algorithms", "Algorithms and data structures", 4, new HashSet<>());

        mockMvc.perform(post("/course/create")
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(course)))
                .andExpect(status().isOk());

        Course dbEntry = courseService.findById(course.getId());
        assertEquals(course, dbEntry);
    }

    @Test
    void testCreateCourseFailure() throws Exception {
        Course course = new Course(UUID.fromString("bde8452f-ea24-44ca-9f0f-e99258b105d7"), "Algorithms", "Algorithms and data structures", 4, new HashSet<>());

        courseService.save(course);

        mockMvc.perform(post("/course/create")
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(course)))
                .andExpect(status().is(COURSE_IS_EXIST.getCode()))
                .andExpect(jsonPath("code").value(COURSE_IS_EXIST.getCode()))
                .andExpect(jsonPath("message").value(COURSE_IS_EXIST.getMessage()));
    }

    @Test
    void testUpdateCourseSuccess() throws Exception {
        Course course = new Course(UUID.fromString("bde8452f-ea24-44ca-9f0f-e99258b105d7"), "Algorithms", "Algorithms and data structures", 4, null);

        courseService.save(course);

        course.setDescription("Algorithms on strings");

        mockMvc.perform(put("/course/update")
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(course)))
                .andExpect(status().isOk());
    }

    @Test
    void testUpdateCourseFailure() throws Exception {
        Course course = new Course(UUID.randomUUID(), "Algorithms", "Algorithms and data structures", 4, null);

        mockMvc.perform(put("/course/update")
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(course)))
                .andExpect(status().is(COURSE_NOT_FOUND.getCode()))
                .andExpect(jsonPath("code").value(COURSE_NOT_FOUND.getCode()))
                .andExpect(jsonPath("message").value(COURSE_NOT_FOUND.getMessage()));
    }

    @Test
    void testGetCourseSuccess() throws Exception {
        Course course = new Course(UUID.fromString("bde8452f-ea24-44ca-9f0f-e99258b105d7"), "Algorithms", "Algorithms and data structures", 4, new HashSet<>());

        courseService.save(course);

        mockMvc.perform(get("/course/get")
                        .param("id", course.getId().toString()))
                .andExpect(status().isOk())
                .andExpect(content().bytes(convertObjectToJsonBytes(course)));
    }

    @Test
    void testGetCourseFailure() throws Exception {
        mockMvc.perform(get("/course/get")
                        .param("id", UUID.randomUUID().toString()))
                .andExpect(status().is(COURSE_NOT_FOUND.getCode()))
                .andExpect(jsonPath("code").value(COURSE_NOT_FOUND.getCode()))
                .andExpect(jsonPath("message").value(COURSE_NOT_FOUND.getMessage()));
    }

    @Test
    void testDeleteCourseSuccess() throws Exception {
        Course course = new Course(UUID.fromString("bde8452f-ea24-44ca-9f0f-e99258b105d7"), "Algorithms", "Algorithms and data structures", 4, new HashSet<>());

        courseService.save(course);

        mockMvc.perform(delete("/course/delete")
                        .param("id", course.getId().toString()))
                .andExpect(status().isOk());

        assertFalse(courseCache.getCourse(course.getId()).isPresent());
        assertFalse(courseRepository.findById(course.getId()).isPresent());
    }

    @Test
    void testDeleteCourseFailure() throws Exception {
        mockMvc.perform(delete("/course/delete")
                        .param("id", UUID.randomUUID().toString()))
                .andExpect(status().is(COURSE_NOT_FOUND.getCode()))
                .andExpect(jsonPath("code").value(COURSE_NOT_FOUND.getCode()))
                .andExpect(jsonPath("message").value(COURSE_NOT_FOUND.getMessage()));
    }

    @Test
    void testAddStudentSuccess() throws Exception {
        Course course = new Course(UUID.fromString("bde8452f-ea24-44ca-9f0f-e99258b105d7"), "Algorithms", "Algorithms and data structures", 4, new HashSet<>());
        Student student = new Student(UUID.fromString("cde8452f-ea24-44ca-9f0f-e99258b105d7"), "Kirill", 20, 5, new HashSet<>());

        courseService.save(course);
        studentService.save(student);

        course.setStudents(Set.of(student));

        mockMvc.perform(put("/course/add_students")
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(course)))
                .andExpect(status().isOk());
    }

    @Test
    void testAddStudentFailure() throws Exception {
        Student student = new Student(UUID.fromString("cde8452f-ea24-44ca-9f0f-e99258b105d7"), "Kirill", 20, 4, new HashSet<>());
        Course course = new Course(UUID.fromString("bde8452f-ea24-44ca-9f0f-e99258b105d7"), "Algorithms", "Algorithms and data structures", 5, new HashSet<>());

        courseService.save(course);
        studentService.save(student);

        course.setStudents(Set.of(student));

        mockMvc.perform(put("/course/add_students")
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(course)))
                .andExpect(status().is(ADDING_STUDENT_NOT_POSSIBLE.getCode()))
                .andExpect(jsonPath("code").value(ADDING_STUDENT_NOT_POSSIBLE.getCode()))
                .andExpect(jsonPath("message").value(ADDING_STUDENT_NOT_POSSIBLE.getMessage()));
    }
}
