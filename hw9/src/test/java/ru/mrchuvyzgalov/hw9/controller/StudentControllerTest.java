package ru.mrchuvyzgalov.hw9.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.web.servlet.MockMvc;
import ru.mrchuvyzgalov.hw9.AbstractTest;
import ru.mrchuvyzgalov.hw9.dao.StudentRepository;
import ru.mrchuvyzgalov.hw9.model.Student;
import ru.mrchuvyzgalov.hw9.service.StudentService;

import java.util.HashSet;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static ru.mrchuvyzgalov.hw9.exception.ApplicationExceptions.STUDENT_IS_EXIST;
import static ru.mrchuvyzgalov.hw9.exception.ApplicationExceptions.STUDENT_NOT_FOUND;

@AutoConfigureMockMvc
public class StudentControllerTest extends AbstractTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private StudentService studentService;

    @Test
    void testCreateStudentSuccess() throws Exception {
        Student student = new Student(UUID.fromString("bde8452f-ea24-44ca-9f0f-e99258b105d7"), "Кирилл", 20, 5, new HashSet<>());

        mockMvc.perform(post("/student/create")
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(student)))
                .andExpect(status().isOk());

        Student dbEntry = studentService.findById(student.getId());
        assertEquals(student, dbEntry);
    }

    @Test
    void testCreateStudentFailure() throws Exception {
        Student student = new Student(UUID.fromString("bde8452f-ea24-44ca-9f0f-e99258b105d7"), "Кирилл", 20, 5, new HashSet<>());

        studentService.save(student);

        mockMvc.perform(post("/student/create")
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(student)))
                .andExpect(status().is(STUDENT_IS_EXIST.getCode()))
                .andExpect(jsonPath("code").value(STUDENT_IS_EXIST.getCode()))
                .andExpect(jsonPath("message").value(STUDENT_IS_EXIST.getMessage()));
    }

    @Test
    void testUpdateStudentSuccess() throws Exception {
        Student student = new Student(UUID.fromString("bde8452f-ea24-44ca-9f0f-e99258b105d7"), "Кирилл", 20, 5, new HashSet<>());

        studentService.save(student);

        student.setAge(21);

        mockMvc.perform(put("/student/update")
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(student)))
                .andExpect(status().isOk());
    }

    @Test
    void testUpdateStudentFailure() throws Exception {
        Student student = new Student(UUID.randomUUID(), "Кирилл", 20, 5, new HashSet<>());

        mockMvc.perform(put("/student/update")
                        .contentType("application/json")
                        .content(convertObjectToJsonBytes(student)))
                .andExpect(status().is(STUDENT_NOT_FOUND.getCode()))
                .andExpect(jsonPath("message").value(STUDENT_NOT_FOUND.getMessage()))
                .andExpect(jsonPath("code").value(STUDENT_NOT_FOUND.getCode()));
    }

    @Test
    void testFindByIdSuccess() throws Exception {
        Student student = new Student(UUID.fromString("bde8452f-ea24-44ca-9f0f-e99258b105d7"), "Кирилл", 20, 5, new HashSet<>());

        studentService.save(student);

        mockMvc.perform(get("/student/get")
                        .param("id", student.getId().toString()))
                .andExpect(status().isOk())
                .andExpect(content().bytes(convertObjectToJsonBytes(student)));
    }

    @Test
    void testFindByIdFailure() throws Exception {
        mockMvc.perform(get("/student/get")
                        .param("id", UUID.randomUUID().toString()))
                .andExpect(status().is(STUDENT_NOT_FOUND.getCode()))
                .andExpect(jsonPath("message").value(STUDENT_NOT_FOUND.getMessage()))
                .andExpect(jsonPath("code").value(STUDENT_NOT_FOUND.getCode()));
    }

    @Test
    void testDeleteByIdSuccess() throws Exception {
        Student student = new Student(UUID.fromString("bde8452f-ea24-44ca-9f0f-e99258b105d7"), "Кирилл", 20, 5, new HashSet<>());

        studentService.save(student);

        mockMvc.perform(delete("/student/delete")
                        .param("id", student.getId().toString()))
                .andExpect(status().isOk());

        assertFalse(studentRepository.findById(student.getId()).isPresent());
    }

    @Test
    void testDeleteByIdFailure() throws Exception {
        mockMvc.perform(delete("/student/delete")
                        .param("id", UUID.randomUUID().toString()))
                .andExpect(status().is(STUDENT_NOT_FOUND.getCode()))
                .andExpect(jsonPath("message").value(STUDENT_NOT_FOUND.getMessage()))
                .andExpect(jsonPath("code").value(STUDENT_NOT_FOUND.getCode()));
    }
}
