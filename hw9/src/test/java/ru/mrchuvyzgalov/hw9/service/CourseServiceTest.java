package ru.mrchuvyzgalov.hw9.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.mrchuvyzgalov.hw9.AbstractTest;
import ru.mrchuvyzgalov.hw9.cache.CourseCache;
import ru.mrchuvyzgalov.hw9.dao.CourseRepository;
import ru.mrchuvyzgalov.hw9.exception.ApplicationExceptions.ApplicationException;
import ru.mrchuvyzgalov.hw9.model.Course;

import java.util.HashSet;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

public class CourseServiceTest extends AbstractTest {
    @Autowired
    private CourseCache courseCache;
    @Autowired
    private CourseService courseService;
    @Autowired
    private CourseRepository courseRepository;

    @Test
    public void addCourseSuccess() {
        Course course = new Course(UUID.randomUUID(), "Math", "Linear algebra", 4, new HashSet<>());

        courseService.save(course);

        assertEquals(0, courseCache.size());
    }

    @Test
    public void addCourseFailure() {
        Course course = new Course(UUID.randomUUID(), "Math", "Linear algebra", 4, new HashSet<>());
        courseRepository.save(course);

        Throwable thrown = assertThrows(ApplicationException.class, () -> {
            courseService.save(course);
        });
        assertNotNull(thrown.getMessage());

        assertEquals(0, courseCache.size());
    }

    @Test
    public void updateCourseSuccess() {
        Course course = new Course(UUID.randomUUID(), "Math", "Linear algebra", 4, new HashSet<>());
        courseRepository.save(course);

        course.setReqiredGrade(5);
        courseService.updateById(course);

        assertEquals(0, courseCache.size());
    }

    @Test
    public void updateCourseFailure() {
        Course course = new Course(UUID.randomUUID(), "Math", "Linear algebra", 4, new HashSet<>());

        Throwable thrown = assertThrows(ApplicationException.class, () -> {
            courseService.updateById(course);
        });
        assertNotNull(thrown.getMessage());

        assertEquals(0, courseCache.size());
    }

    @Test
    public void deleteCourseSuccess() {
        Course course = new Course(UUID.randomUUID(), "Math", "Linear algebra", 4, new HashSet<>());
        courseRepository.save(course);
        courseCache.add(course);

        courseService.deleteById(course.getId());

        assertEquals(0, courseCache.size());
    }

    @Test
    public void deleteCourseWhereCacheHasAnotherCoursesSuccess() {
        Course oldCourse = new Course(UUID.randomUUID(), "Old", "Old", 3, new HashSet<>());
        courseCache.add(oldCourse);
        courseRepository.save(oldCourse);

        Course course = new Course(UUID.randomUUID(), "Math", "Linear algebra", 4, new HashSet<>());
        courseRepository.save(course);
        courseCache.add(course);

        courseService.deleteById(course.getId());

        assertEquals(1, courseCache.size());
        assertEquals(oldCourse, courseCache.getCourse(oldCourse.getId()).get());
    }

    @Test
    public void deleteCourseFailure() {
        Throwable thrown = assertThrows(ApplicationException.class, () -> {
            courseService.deleteById(UUID.randomUUID());
        });
        assertNotNull(thrown.getMessage());

        assertEquals(0, courseCache.size());
    }

    @Test
    public void getCourseSuccess() {
        Course course = new Course(UUID.randomUUID(), "Math", "Linear algebra", 4, new HashSet<>());
        courseRepository.save(course);

        assertEquals(0, courseCache.size());

        assertEquals(course, courseService.findById(course.getId()));
        assertEquals(1, courseCache.size());
        assertEquals(course, courseCache.getCourse(course.getId()).get());
    }

    @Test
    public void getCourseFromCacheSuccess() {
        Course course = new Course(UUID.randomUUID(), "Math", "Linear algebra", 4, new HashSet<>());
        courseCache.add(course);

        assertFalse(courseRepository.findByIdWithoutStudents(course.getId()).isPresent());

        assertEquals(course, courseService.findById(course.getId()));
        assertEquals(1, courseCache.size());
        assertEquals(course, courseCache.getCourse(course.getId()).get());
    }

    @Test
    public void getCourseWhereCacheHasAnotherCoursesSuccess() {
        Course oldCourse = new Course(UUID.randomUUID(), "Old", "Old", 3, new HashSet<>());
        courseCache.add(oldCourse);
        courseRepository.save(oldCourse);

        Course course = new Course(UUID.randomUUID(), "Math", "Linear algebra", 4, new HashSet<>());
        courseRepository.save(course);

        assertEquals(1, courseCache.size());

        assertEquals(course, courseService.findById(course.getId()));
        assertEquals(2, courseCache.size());
        assertEquals(course, courseCache.getCourse(course.getId()).get());
    }

    @Test
    public void getCourseFailure() {
        Throwable thrown = assertThrows(ApplicationException.class, () -> {
            courseService.findById(UUID.randomUUID());
        });
        assertNotNull(thrown.getMessage());

        assertEquals(0, courseCache.size());
    }
}
