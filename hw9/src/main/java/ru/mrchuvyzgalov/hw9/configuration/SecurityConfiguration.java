package ru.mrchuvyzgalov.hw9.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    @Value("${admin.username}")
    private String adminUsername;
    @Value("${admin.password}")
    private String adminPassword;

    @Value("${user.username}")
    private String userUsername;
    @Value("${user.password}")
    private String userPassword;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication().withUser(adminUsername).password("{noop}" + adminPassword).roles("ADMIN");
        auth.inMemoryAuthentication().withUser(userUsername).password("{noop}" + userPassword).roles("USER");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/student/get").permitAll()
                .antMatchers("/course/get").permitAll()
                .antMatchers("/**").hasRole("ADMIN")
                .anyRequest().authenticated().and().httpBasic();
    }
}
