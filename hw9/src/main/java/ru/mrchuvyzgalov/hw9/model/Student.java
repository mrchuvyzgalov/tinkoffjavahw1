package ru.mrchuvyzgalov.hw9.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.jdbc.core.RowMapper;

import javax.validation.constraints.NotNull;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Student {
    @NotNull
    private UUID id;
    @NotNull
    private String name;
    @NotNull
    private Integer age;
    @NotNull
    private Integer grade;
    @NotNull
    private Set<Course> courses;

    public static Mapper mapper = new Mapper();

    private static class Mapper implements RowMapper<Student> {

        @Override
        public Student mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Student(
                    UUID.fromString(rs.getString("id")),
                    rs.getString("name"),
                    rs.getInt("age"),
                    rs.getInt("grade"),
                    new HashSet<>());
        }
    }
}