package ru.mrchuvyzgalov.hw9.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.mrchuvyzgalov.hw9.cache.CourseCache;
import ru.mrchuvyzgalov.hw9.dao.CourseRepository;
import ru.mrchuvyzgalov.hw9.model.Course;
import ru.mrchuvyzgalov.hw9.model.Student;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static ru.mrchuvyzgalov.hw9.exception.ApplicationExceptions.*;

@Service
@AllArgsConstructor
public class CourseService {
    private final CourseRepository courseRepository;
    private final StudentService studentService;
    private CourseCache courseCache;

    public void save(Course course) {
        if (containsInCache(course.getId()) || containsInDb(course.getId())) {
            throw COURSE_IS_EXIST.exception();
        }

        courseRepository.save(course);
    }

    private boolean containsInDb(UUID id) {
        return courseRepository.findByIdWithoutStudents(id).isPresent();
    }

    private boolean containsInCache(UUID id) {
        return courseCache.getCourse(id).isPresent();
    }

    private boolean containsWithStudentsInDb(UUID id) {
        return courseRepository.findById(id).isPresent();
    }

    private boolean containsWithStudentsInCache(UUID id) {
        Optional<Course> optionalCourseCache = courseCache.getCourse(id);

        if (optionalCourseCache.isPresent()) {
            Set<Student> students = optionalCourseCache.get().getStudents();
            if (students != null && students.size() > 0) {
                return true;
            }
        }

        return false;
    }

    public Course findById(UUID id) {
        if (containsInCache(id)) {
            return courseCache.getCourse(id).get();
        }

        if (!containsInDb(id)) {
            throw COURSE_NOT_FOUND.exception();
        }

        Course course = containsWithStudentsInDb(id) ? courseRepository.findById(id).get() : courseRepository.findByIdWithoutStudents(id).get();
        courseCache.add(course);
        return course;
    }

    public List<Course> findAll() {
        return courseRepository.findAll();
    }

    public Course findCourseWithTheHighestAverageAgeOfStudents() {
        return courseRepository.findCourseWithTheHighestAverageAgeOfStudents().orElseThrow();
    }

    public void updateById(Course course) {
        if (!containsInCache(course.getId())) {
            if (!containsInDb(course.getId())) {
                throw COURSE_NOT_FOUND.exception();
            }
        }
        else {
            courseCache.deleteById(course.getId());
        }
        courseRepository.updateById(course);
    }

    public void addStudentsToCourse(Course course) {
        if (!containsInCache(course.getId()) && !containsInDb(course.getId())) {
            throw COURSE_NOT_FOUND.exception();
        }

        for (Student student : course.getStudents()) {
            if (!studentService.contains(student.getId())) {
                throw STUDENT_NOT_FOUND.exception();
            }
        }

        for (Student student : course.getStudents()) {
            Student studentFromDb = studentService.findById(student.getId());
            studentFromDb.getCourses().add(course);

            studentService.updateById(studentFromDb);
        }

        courseCache.deleteById(course.getId());
    }

    public void deleteById(UUID id) {
        if (!containsInCache(id) && !containsInDb(id)) {
            throw COURSE_NOT_FOUND.exception();
        }
        courseCache.deleteById(id);
        courseRepository.deleteById(id);
    }

    public void deleteAll() {
        courseCache.clear();
        courseRepository.deleteAll();
    }
}

