package ru.mrchuvyzgalov.hw9.cache;

import org.springframework.stereotype.Component;
import ru.mrchuvyzgalov.hw9.model.Course;

import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Component
public class CourseCache {
    private ConcurrentMap<UUID, Course> cache = new ConcurrentHashMap<>();

    public Optional<Course> getCourse(UUID id) {
        if (cache.containsKey(id)) {
            return Optional.of(cache.get(id));
        }
        return Optional.empty();
    }

    public void add(Course course) {
        cache.put(course.getId(), course);
    }

    public void deleteById(UUID id) {
        if (cache.containsKey(id)) {
            cache.remove(id);
        }
    }

    public void clear() {
        cache.clear();
    }

    public int size() {
        return cache.size();
    }
}
