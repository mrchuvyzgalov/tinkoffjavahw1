package ru.mrchuvyzgalov.hw9.exception;

import lombok.Getter;

@Getter
public enum ApplicationExceptions {

    STUDENT_NOT_FOUND("student not found", 404),
    STUDENT_IS_EXIST("student is exist", 404),
    COURSE_NOT_FOUND("course not found", 404),
    COURSE_IS_EXIST("course is exist", 404),
    ADDING_STUDENT_NOT_POSSIBLE("The student's grade is less than the recommended course grade", 404);

    private final String message;
    private final int code;

    ApplicationExceptions(String message, int code) {
        this.message = message;
        this.code = code;
    }

    public ApplicationException exception() {
        return new ApplicationException(this);
    }

    public static class ApplicationException extends RuntimeException {
        public final ApplicationExceptionCompanion companion;

        public ApplicationException(ApplicationExceptions error) {
            super(error.message);
            this.companion = new ApplicationExceptionCompanion(error.code, error.message);
        }

        public static record ApplicationExceptionCompanion(int code, String message) { }
    }
}
