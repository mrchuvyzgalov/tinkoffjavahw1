package ru.mrchuvyzgalov.hw3;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Aspect
public class StudentStatistic {
    private Map<Student, Integer> numberOfCalls = new HashMap<>();

    @AfterReturning(pointcut = "@annotation(ru.mrchuvyzgalov.hw3.CountStudents)", returning = "students")
    public void afterReturningGetStudents(List<Student> students) {
        for (Student student : students) {
            if (!numberOfCalls.containsKey(student)) {
                numberOfCalls.put(student, 0);
            }
            int calls = numberOfCalls.get(student);
            numberOfCalls.put(student, calls + 1);
        }
    }
}
