package ru.mrchuvyzgalov.hw3;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.*;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

@Component
@EnableScheduling
public class Task {
    private final List<Student> students = new ArrayList<>();
    private final String path = "students.csv";

    public Task() {
        try(InputStream is = Hw3Application.class.getClassLoader().getResourceAsStream(path);
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr)) {

            List<String> lines =  br.lines().collect(Collectors.toList());

            for (int id = 1; id < lines.size(); ++id) {
                String[] elems = lines.get(id).split(",");

                String name = elems[0].substring(1, elems[0].length() - 1);
                int age = Integer.parseInt(elems[1]);
                int timeFrom = Integer.parseInt(elems[2]);
                int timeTo = Integer.parseInt(elems[3]);

                students.add(new Student(id, name, age, timeFrom, timeTo));;
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    @CountStudents
    @Scheduled(fixedRate = 3600000)
    public List<Student> getBusyStudents() throws NoSuchMethodException {
        Date date = new Date();
        int hours = date.getHours();

        return students.stream()
                .filter(student -> student.getTimeFrom() <= hours && hours <= student.getTimeTo())
                .collect(Collectors.toList());
    }
}
