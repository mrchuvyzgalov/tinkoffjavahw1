package ru.mrchuvyzgalov.hw7.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.mrchuvyzgalov.hw7.dao.CourseRepository;
import ru.mrchuvyzgalov.hw7.model.Course;
import ru.mrchuvyzgalov.hw7.model.Student;

import java.util.List;
import java.util.UUID;

import static ru.mrchuvyzgalov.hw7.exception.ApplicationExceptions.*;

@Service
@AllArgsConstructor
public class CourseService {
    private final CourseRepository courseRepository;
    private final StudentService studentService;

    public void save(Course course) {
        if (contains(course.getId())) {
            throw COURSE_IS_EXIST.exception();
        }
        courseRepository.save(course);
    }

    public boolean contains(UUID id) {
        return courseRepository.findByIdWithoutStudents(id).isPresent();
    }

    public boolean containsWithStudents(UUID id) {
        return courseRepository.findById(id).isPresent();
    }

    public Course findById(UUID id) {
        if (!contains(id)) {
            throw COURSE_NOT_FOUND.exception();
        }

        if (containsWithStudents(id)) {
            return courseRepository.findById(id).get();
        }
        else {
            return courseRepository.findByIdWithoutStudents(id).get();
        }
    }

    public List<Course> findAll() {
        return courseRepository.findAll();
    }

    public Course findCourseWithTheHighestAverageAgeOfStudents() {
        return courseRepository.findCourseWithTheHighestAverageAgeOfStudents().orElseThrow();
    }

    public void updateById(Course course) {
        if (!contains(course.getId())) {
            throw COURSE_NOT_FOUND.exception();
        }
        courseRepository.updateById(course);
    }

    public void addStudentsToCourse(Course course) {
        if (!contains(course.getId())) {
            throw COURSE_NOT_FOUND.exception();
        }

        for (Student student : course.getStudents()) {
            if (!studentService.contains(student.getId())) {
                throw STUDENT_NOT_FOUND.exception();
            }
        }

        for (Student student : course.getStudents()) {
            Student studentFromDb = studentService.findById(student.getId());
            studentFromDb.getCourses().add(course);

            studentService.updateById(studentFromDb);
        }
    }

    public void deleteById(UUID id) {
        if (!contains(id)) {
            throw COURSE_NOT_FOUND.exception();
        }
        courseRepository.deleteById(id);
    }

    public void deleteAll() {
        courseRepository.deleteAll();
    }
}

