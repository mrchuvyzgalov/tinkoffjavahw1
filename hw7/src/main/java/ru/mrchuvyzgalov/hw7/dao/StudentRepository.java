package ru.mrchuvyzgalov.hw7.dao;

import org.apache.ibatis.annotations.Mapper;
import ru.mrchuvyzgalov.hw7.model.Student;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Mapper
public interface StudentRepository {
    void save(Student student);
    void saveWithoutCourses(Student student);

    Optional<Student> findById(UUID id);
    Optional<Student> findByIdWithoutCourses(UUID id);

    List<Student> findAll();

    void updateById(Student student);

    void deleteById(UUID id);
    void deleteAll();
}

