package ru.mrchuvyzgalov.hw10.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import ru.mrchuvyzgalov.hw10.model.Student;

import java.nio.charset.StandardCharsets;

@Service
public class ProducerService {
    @Value("${kafka.topic}")
    private String topic;

    @Autowired
    private KafkaTemplate<String, byte[]> kafkaTemplate;

    public void produce(Student student) {
        kafkaTemplate.send(topic, student.toString().getBytes(StandardCharsets.UTF_16));
    }
}
