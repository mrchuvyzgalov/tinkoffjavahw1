package ru.mrchuvyzgalov;

import org.junit.Assert;
import org.junit.Test;

import java.util.*;

import static ru.mrchuvyzgalov.Main.*;

public class MainTest {
    private List<Student> students;

    public MainTest() {
        students = new ArrayList<>();

        students.add(new Student(1, "Кирилл", 20));
        students.add(new Student(2, "Кирилл", 18));
        students.add(new Student(3, "Полина", 19));
        students.add(new Student(4, "Вика", 21));
        students.add(new Student(5, "Алина", 20));
        students.add(new Student(6, "Ольга", 20));
        students.add(new Student(7, "Александр", 19));
        students.add(new Student(8, "Лена", 19));
        students.add(new Student(9, "Аня", 20));
        students.add(new Student(10, "Аня", 19));
        students.add(new Student(11, "Костя", 20));
        students.add(new Student(12, "Максим", 20));
        students.add(new Student(13, "Роберт", 18));
        students.add(new Student(14, "Михаил", 20));
        students.add(new Student(15, "Софья", 20));
        students.add(new Student(16, "Паша", 20));
        students.add(new Student(17, "Андрей", 20));
        students.add(new Student(18, "Алена", 20));
    }

    @Test
    public void getTotalAgeByNameTest() {
        int result = getTotalAgeByName(students, "Кирилл");

        Assert.assertEquals(38, result);
    }

    @Test
    public void getSetOfStudentNamesTest() {
        Set<String> result = getSetOfStudentNames(students);

        Set<String> correct = new HashSet<>();

        correct.add("Кирилл");
        correct.add("Полина");
        correct.add("Вика");
        correct.add("Алина");
        correct.add("Ольга");
        correct.add("Александр");
        correct.add("Лена");
        correct.add("Аня");
        correct.add("Костя");
        correct.add("Максим");
        correct.add("Роберт");
        correct.add("Михаил");
        correct.add("Софья");
        correct.add("Паша");
        correct.add("Андрей");
        correct.add("Алена");

        Assert.assertEquals(correct, result);
    }

    @Test
    public void hasAnOlderStudentTest() {
        boolean result = hasAnOlderStudent(students, 20);

        Assert.assertTrue(result);
    }

    @Test
    public void getMapByIdAndNameTest() {
        Map<Long, String> result = getMapByIdAndName(students);

        Map<Long, String> correct = new HashMap<>();

        correct.put(1L, "Кирилл");
        correct.put(2L, "Кирилл");
        correct.put(3L, "Полина");
        correct.put(4L, "Вика");
        correct.put(5L, "Алина");
        correct.put(6L, "Ольга");
        correct.put(7L, "Александр");
        correct.put(8L, "Лена");
        correct.put(9L, "Аня");
        correct.put(10L, "Аня");
        correct.put(11L, "Костя");
        correct.put(12L, "Максим");
        correct.put(13L, "Роберт");
        correct.put(14L, "Михаил");
        correct.put(15L, "Софья");
        correct.put(16L, "Паша");
        correct.put(17L, "Андрей");
        correct.put(18L, "Алена");

        Assert.assertEquals(correct, result);
    }

    @Test
    public void getMapByAgeAndListOfStudentsTest() {
        Map<Integer, List<Student>> result = getMapByAgeAndListOfStudents(students);

        Map<Integer, List<Student>> correct = new HashMap<>();

        List<Student> eighteen = new ArrayList<>();
        eighteen.add(new Student(2, "Кирилл", 18));
        eighteen.add(new Student(13, "Роберт", 18));
        correct.put(18, eighteen);

        List<Student> nineteen = new ArrayList<>();
        nineteen.add(new Student(3, "Полина", 19));
        nineteen.add(new Student(7, "Александр", 19));
        nineteen.add(new Student(8, "Лена", 19));
        nineteen.add(new Student(10, "Аня", 19));
        correct.put(19, nineteen);

        List<Student> twenty = new ArrayList<>();
        twenty.add(new Student(1, "Кирилл", 20));
        twenty.add(new Student(5, "Алина", 20));
        twenty.add(new Student(6, "Ольга", 20));
        twenty.add(new Student(9, "Аня", 20));
        twenty.add(new Student(11, "Костя", 20));
        twenty.add(new Student(12, "Максим", 20));
        twenty.add(new Student(14, "Михаил", 20));
        twenty.add(new Student(15, "Софья", 20));
        twenty.add(new Student(16, "Паша", 20));
        twenty.add(new Student(17, "Андрей", 20));
        twenty.add(new Student(18, "Алена", 20));
        correct.put(20, twenty);

        List<Student> twentyOne = new ArrayList<>();
        twentyOne.add(new Student(4, "Вика", 21));
        correct.put(21, twentyOne);

        Assert.assertEquals(correct, result);
    }
}
