package ru.mrchuvyzgalov;

import java.util.*;
import java.util.stream.Collectors;

public class Main {
    static int getTotalAgeByName(List<Student> students, String name) {
        Map<String, Integer> totalAgeByName = students.stream()
                .collect(Collectors.groupingBy(Student::getName, Collectors.summingInt(Student::getAge)));

        return totalAgeByName.get(name);
    }

    static Set<String> getSetOfStudentNames(List<Student> students) {
        Set<String> nameSet= students.stream()
                .map(Student::getName)
                .collect(Collectors.toSet());

        return nameSet;
    }

    static boolean hasAnOlderStudent(List<Student> students, int age) {
        boolean hasStudent = students.stream()
                .anyMatch(student -> student.getAge() > age);

        return hasStudent;
    }

    static Map<Long, String> getMapByIdAndName(List<Student> students) {
        Map<Long, String> mapByIdAndName = students.stream()
                .collect(Collectors.toMap(Student::getId, Student::getName));

        return mapByIdAndName;
    }

    static Map<Integer, List<Student>> getMapByAgeAndListOfStudents(List<Student> students) {
        Map<Integer, List<Student>> mapByAgeAndListOfStudents = students.stream()
                .collect(Collectors.groupingBy(Student::getAge));

        return mapByAgeAndListOfStudents;
    }

    public static void main( String[] args ) {

    }
}
