package ru.mrchuvyzgalov.hw6.validation;

import ru.mrchuvyzgalov.hw6.model.Course;
import ru.mrchuvyzgalov.hw6.model.Student;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class GradeConstraintValidator implements ConstraintValidator<GradeConstaraint, Course> {

    @Override
    public boolean isValid(Course course, ConstraintValidatorContext constraintValidatorContext) {
        for (Student student : course.getStudents()) {
            if (course.getReqiredGrade() > student.getGrade()) {
                return false;
            }
        }
        return true;
    }
}
