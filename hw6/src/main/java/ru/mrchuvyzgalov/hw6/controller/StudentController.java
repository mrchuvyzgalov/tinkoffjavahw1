package ru.mrchuvyzgalov.hw6.controller;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.mrchuvyzgalov.hw6.model.Student;
import ru.mrchuvyzgalov.hw6.service.StudentService;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/student")
@AllArgsConstructor
public class StudentController {
    private final StudentService studentService;

    @PostMapping("/create")
    public void createNewStudent(@Valid @RequestBody Student student) {
        studentService.save(student);
    }

    @PutMapping("/update")
    public void updateStudent(@Valid @RequestBody Student student) {
        studentService.updateById(student);
    }

    @DeleteMapping("/delete")
    public void deleteStudent(@RequestParam UUID id) {
        studentService.deleteById(id);
    }

    @GetMapping("/get")
    public Student getStudent(@RequestParam UUID id) {
        return studentService.findById(id);
    }
}
