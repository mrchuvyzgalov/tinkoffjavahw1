package ru.mrchuvyzgalov.hw6.controller;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.mrchuvyzgalov.hw6.model.Course;
import ru.mrchuvyzgalov.hw6.service.CourseService;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/course")
@AllArgsConstructor
public class CourseController {
    private final CourseService courseService;

    @PostMapping("/create")
    public void createNewCourse(@Valid @RequestBody Course course) {
        courseService.save(course);
    }

    @PutMapping("/update")
    public void updateCourse(@Valid @RequestBody Course course) {
        courseService.updateById(course);
    }

    @DeleteMapping("/delete")
    public void deleteCourse(@RequestParam UUID id) {
        courseService.deleteById(id);
    }

    @GetMapping("/get")
    public Course getCourse(@RequestParam UUID id) {
        return courseService.findById(id);
    }

    @PutMapping("/add_students")
    public void addStudent(@Valid @RequestBody Course course) {
        courseService.addStudentsToCourse(course);
    }
}
