package ru.mrchuvyzgalov.hw6.exception;

public enum ApplicationExceptions {

    STUDENT_NOT_FOUND("student not found", 404),
    COURSE_NOT_FOUND("course not found", 404),
    ADDING_STUDENT_NOT_POSSIBLE("The student cannot be added to the course", 404);

    private final String message;
    private final int code;

    ApplicationExceptions(String message, int code) {
        this.message = message;
        this.code = code;
    }

    public ApplicationException exception() {
        return new ApplicationException(this);
    }

    public static class ApplicationException extends RuntimeException {
        public final ApplicationExceptionCompanion companion;

        public ApplicationException(ApplicationExceptions error) {
            super(error.message);
            this.companion = new ApplicationExceptionCompanion(error.code, error.message);
        }

        public static record ApplicationExceptionCompanion(int code, String message) { }
    }
}
