package ru.mrchuvyzgalov.hw6.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.mrchuvyzgalov.hw6.dao.CourseRepository;
import ru.mrchuvyzgalov.hw6.model.Course;
import ru.mrchuvyzgalov.hw6.model.Student;

import java.util.List;
import java.util.UUID;

import static ru.mrchuvyzgalov.hw6.exception.ApplicationExceptions.COURSE_NOT_FOUND;
import static ru.mrchuvyzgalov.hw6.exception.ApplicationExceptions.STUDENT_NOT_FOUND;

@Service
@AllArgsConstructor
public class CourseService {
    private final CourseRepository courseRepository;
    private final StudentService studentService;

    public void save(Course course) {
        courseRepository.save(course);
    }

    public boolean contains(UUID id) {
        return courseRepository.findById(id).isPresent();
    }

    public Course findById(UUID id) {
        if (!contains(id)) {
            throw COURSE_NOT_FOUND.exception();
        }
        return courseRepository.findById(id).get();
    }

    public List<Course> findAll() {
        return courseRepository.findAll();
    }

    public Course findCourseWithTheHighestAverageAgeOfStudents() {
        return courseRepository.findCourseWithTheHighestAverageAgeOfStudents().orElseThrow();
    }

    public void updateById(Course course) {
        if (!contains(course.getId())) {
            throw COURSE_NOT_FOUND.exception();
        }
        courseRepository.updateById(course);
    }

    public void addStudentsToCourse(Course course) {
        if (!contains(course.getId())) {
            throw COURSE_NOT_FOUND.exception();
        }

        for (Student student : course.getStudents()) {
            if (!studentService.contains(student.getId())) {
                throw STUDENT_NOT_FOUND.exception();
            }
        }

        for (Student student : course.getStudents()) {
            Student studentFromDb = studentService.findById(student.getId());
            studentFromDb.getCourses().add(course);

            studentService.updateById(studentFromDb);
        }
    }

    public void deleteById(UUID id) {
        if (!contains(id)) {
            throw COURSE_NOT_FOUND.exception();
        }
        courseRepository.deleteById(id);
    }

    public void deleteAll() {
        courseRepository.deleteAll();
    }
}

