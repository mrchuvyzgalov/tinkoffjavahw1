package ru.mrchuvyzgalov.hw6.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.mrchuvyzgalov.hw6.dao.StudentRepository;
import ru.mrchuvyzgalov.hw6.model.Student;

import java.util.List;
import java.util.UUID;

import static ru.mrchuvyzgalov.hw6.exception.ApplicationExceptions.STUDENT_NOT_FOUND;

@Service
@AllArgsConstructor
public class StudentService {
    private final StudentRepository studentRepository;

    public void save(Student student) {
        studentRepository.save(student);
    }

    public boolean contains(UUID id) {
        return studentRepository.findById(id).isPresent();
    }

    public Student findById(UUID id) {
        if (!contains(id)) {
            throw STUDENT_NOT_FOUND.exception();
        }
        return studentRepository.findById(id).orElseThrow();
    }

    public List<Student> findAll() {
        return studentRepository.findAll();
    }

    public void updateById(Student student) {
        if (!contains(student.getId())) {
            throw STUDENT_NOT_FOUND.exception();
        }
        studentRepository.updateById(student);
    }

    public void deleteById(UUID id) {
        if (!contains(id)) {
            throw STUDENT_NOT_FOUND.exception();
        }
        studentRepository.deleteById(id);
    }

    public void deleteAll() {
        studentRepository.deleteAll();
    }
}

