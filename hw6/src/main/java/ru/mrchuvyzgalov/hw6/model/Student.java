package ru.mrchuvyzgalov.hw6.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.Set;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Student {
    @NotNull
    private UUID id;
    @NotNull
    private String name;
    @NotNull
    private Integer age;
    @NotNull
    private Integer grade;
    @NotNull
    private Set<Course> courses;
}

