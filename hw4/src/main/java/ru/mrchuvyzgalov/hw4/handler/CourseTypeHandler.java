package ru.mrchuvyzgalov.hw4.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import ru.mrchuvyzgalov.hw4.model.Course;

import java.sql.*;

public class CourseTypeHandler extends BaseTypeHandler<Course> {
    private ObjectMapper mapper = new ObjectMapper();

    @SneakyThrows
    @Override
    public void setNonNullParameter(PreparedStatement preparedStatement, int i, Course course, JdbcType jdbcType) throws SQLException {
        preparedStatement.setString(i, mapper.writeValueAsString(course));
    }

    @SneakyThrows
    @Override
    public Course getNullableResult(ResultSet resultSet, String s) throws SQLException {
        String courseJSON = resultSet.getObject(s, String.class);
        return mapper.readValue(courseJSON, Course.class);
    }

    @SneakyThrows
    @Override
    public Course getNullableResult(ResultSet resultSet, int i) throws SQLException {
        String courseJSON = resultSet.getObject(i, String.class);
        return mapper.readValue(courseJSON, Course.class);
    }

    @SneakyThrows
    @Override
    public Course getNullableResult(CallableStatement callableStatement, int i) throws SQLException {
        String courseJSON = callableStatement.getObject(i, String.class);
        return mapper.readValue(courseJSON, Course.class);
    }
}
