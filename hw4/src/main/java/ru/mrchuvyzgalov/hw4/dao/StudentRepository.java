package ru.mrchuvyzgalov.hw4.dao;

import org.apache.ibatis.annotations.Mapper;
import ru.mrchuvyzgalov.hw4.model.Student;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Mapper
public interface StudentRepository {
    void save(Student student);

    Optional<Student> findById(UUID id);
    List<Student> findAll();

    void changeById(Student student);

    void deleteById(UUID id);
    void deleteAll();
}
